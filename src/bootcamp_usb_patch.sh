#!/bin/bash
ORIG_BOOTCAMP_PATH="/Applications/Utilities/Boot Camp Assistant.app"
PATCHED_BOOTCAMP_PATH="$HOME/Desktop/Boot Camp Assistant (Patched).app"

PLIST_HARDWARE="$HOME/Desktop/hardware_info.plist"

system_profiler SPHardwareDataType -xml > "${PLIST_HARDWARE}"

## PLIST PATH
PLIST_SELECTOR=":0:_items:0"

## DEBUG: Print hardware plist
# /usr/libexec/PlistBuddy -c "print $PLIST_SELECTOR" "${PLIST_HARDWARE}"

# Get machine model and boot rom version
machine_model=$(/usr/libexec/PlistBuddy -c "print  ${PLIST_SELECTOR}:machine_model" "${PLIST_HARDWARE}")
boot_rom_version=$(/usr/libexec/PlistBuddy -c "print  ${PLIST_SELECTOR}:boot_rom_version" "${PLIST_HARDWARE}")

#echo "Creating patched version of Bootcamp at location '${PATCHED_BOOTCAMP_PATH}'"



rm -rf "${PATCHED_BOOTCAMP_PATH}"

echo "   Copying Bootcamp application to Desktop"
cp -r "${ORIG_BOOTCAMP_PATH}" "${PATCHED_BOOTCAMP_PATH}"

echo ""
echo "   Patching BootCamp copy for machine model: $machine_model, boot rom version: $boot_rom_version"

####### Patch Info.plist in new bootcamp location ########

# Step 1: Add current boot rom to DARequiredROMVersions
echo "      Step 1: Add current boot rom to DARequiredROMVersions"
/usr/libexec/PlistBuddy -c "Add :DARequiredROMVersions: string '${boot_rom_version}'" "${PATCHED_BOOTCAMP_PATH}/Contents/Info.plist"

### Step 2: Delete the word "Pre" from UEFIModels and add your model
# Step 2.1 Rename PreUEFIModels to UEFIModels
echo "      Step 2: Delete the word \"Pre\" from UEFIModels and add your model"
/usr/libexec/PlistBuddy -c "Copy :PreUEFIModels :UEFIModels" "${PATCHED_BOOTCAMP_PATH}/Contents/Info.plist"
/usr/libexec/PlistBuddy -c "Delete :PreUEFIModels" "${PATCHED_BOOTCAMP_PATH}/Contents/Info.plist"

# Step 2.2 Add your model to UEFIModels
/usr/libexec/PlistBuddy -c "Add :UEFIModels: string '${machine_model}'" "${PATCHED_BOOTCAMP_PATH}/Contents/Info.plist"

### Step 3: Delete the word "Pre" from USBBootSupportedModels and add your model
echo "      Step 3: Delete the word \"Pre\" from USBBootSupportedModels and add your model"
# Step 3.1 Rename PreUSBBootSupportedModels to USBBootSupportedModels
/usr/libexec/PlistBuddy -c "Copy :PreUSBBootSupportedModels :USBBootSupportedModels" "${PATCHED_BOOTCAMP_PATH}/Contents/Info.plist"
/usr/libexec/PlistBuddy -c "Delete :PreUSBBootSupportedModels" "${PATCHED_BOOTCAMP_PATH}/Contents/Info.plist"

# Step 3.2 Add your model to USBBootSupportedModels
/usr/libexec/PlistBuddy -c "Add :USBBootSupportedModels: string '${machine_model}'" "${PATCHED_BOOTCAMP_PATH}/Contents/Info.plist"

### Step 4: Remove your model from Win7OnlyModels (if its there)
echo "      Step 4: Remove your model from Win7OnlyModels (if its there)"
## TODO

echo "   Completed patching Info.plist"

## DEBUG: Print complete plist
#/usr/libexec/PlistBuddy -c "Print :" "${PATCHED_BOOTCAMP_PATH}/Contents/Info.plist"

## Resign the patched application
echo ""
echo "   Codesign the patched application..."
codesign -fs - "${PATCHED_BOOTCAMP_PATH}"
if [ $? != 0 ]; then
	echo "Failed signing patched Bootcamp application"
	exit 1
fi
echo "   Codesign completed successfully"

## Verify the patched application is correctly signed
echo ""
#echo "Verify code signature"
codesign -v "${PATCHED_BOOTCAMP_PATH}"
if [ $? = 0 ]; then 
   echo ""
   echo "   Boot Camp Assistant patched successfully"
   echo ""
   echo "   Open '${PATCHED_BOOTCAMP_PATH}' to launch the patched Bootcamp application"
else 
   echo ""
   echo "   Failed verifying patched bootcamp application in location '${PATCHED_BOOTCAMP_PATH}'"
fi